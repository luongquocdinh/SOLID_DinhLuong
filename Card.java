/**
 * Created by quocdinh on 30/6/2016
 */

interface Card {
	public void depositCard(int denominations);
}
