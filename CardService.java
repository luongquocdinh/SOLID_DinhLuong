/**
 * Created by quocdinh on 30/06/2016.
 */
public interface CardService {
    public void increaseBalance(String network, int denominations);
}
